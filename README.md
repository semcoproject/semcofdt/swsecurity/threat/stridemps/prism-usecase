# PRISM Models for Cranes Orchestration in MPS



## Getting started

- Donwload a last version of PRISM 4.7.
- Queries in TamperingAndSpoofingProperties.pctl are associated to PRISM Models (*.nm).
- State-space explosion are studied for buffer size in ModelWithBufferSizeThree.nm ModelWithBufferSizeTwo.nm and ModelWithBufferSizeOne.nm.

## Add your files

- [ ] Load  a model to PRISM and trace a path for data observation
- [ ] Load PCTL files (*.pctl) to check the the models (*.nm).